# State of Drupal presentation (April 2019)

*DrupalCon Seattle Driesnote presentation*

Last week, many Drupalists gathered in Seattle for DrupalCon North America, for what was the largest DrupalCon in history.

As a matter of tradition, I presented my [State of Drupal keynote](https://dri.es/tag/state-of-drupal). You can watch a recording of my keynote (starting at 32 minutes) or [download a copy of my slides](https://dri.es/files/state-of-drupal-april-2019.pdf) (153 MB).

[![DrupalCon Seattle 2019: Driesnote](https://img.youtube.com/vi/Nf_aD3dTloY/0.jpg)](https://www.youtube.com/watch?v=Nf_aD3dTloY "DrupalCon Seattle 2019: Driesnote")

## Making Drupal more diverse and inclusive

DrupalCon Seattle was not only the largest, but also had the most diverse speakers. Nearly 50% of the DrupalCon speakers were from underrepresented groups. This number has been growing year over year, and is something to be proud of.

I actually started my keynote by talking about how we can make Drupal more diverse and inclusive. As one of the largest and most thriving Open Source communities, I believe that Drupal has an obligation to set a positive example.

![Free time to contribute is a privilege](free-time-to-contribute-is-a-privilege-1280w.jpg "Free time to contribute is a privilege")

I talked about how Open Source communities often incorrectly believe that everyone can contribute. Unfortunately, not everyone has equal amounts of [free time to contribute](https://dri.es/the-privilege-of-free-time-in-open-source). In my keynote, I encouraged individuals and organizations in the Drupal community to strongly consider giving time to underrepresented groups.

Improving diversity is not only good for Drupal and its ecosystem, it's good for people, and it's the right thing to do. Because this topic is so important, I wrote [a dedicated blog post](https://dri.es/the-privilege-of-free-time-in-open-source) about it.

## Drupal 8 innovation update

I dedicated a significant portion of my keynote to Drupal 8. In the past year alone, there have been 35% more sites and 48% more stable modules in Drupal 8. Our pace of innovation is increasing, and we've seen important progress in several key areas.

With the release of Drupal 8.7, the Layout Builder will become stable. Drupal's new **Layout Builder** makes it much easier to build and change one-off page layouts, templated layouts and layout workflows. Best of all, [the Layout Builder will be accessible](https://dri.es/drupal-commitment-to-accessibility).

[![Layout Builder in Drupal 8.7](https://img.youtube.com/vi/oB28lfIUcNU/0.jpg)](https://www.youtube.com/watch?v=oB28lfIUcNU "Layout Builder in Drupal 8.7")

Drupal 8.7 also brings a lot of improvements to the **Media Library**.

[![Media Library in Drupal 8.7](https://img.youtube.com/vi/R3DT99QllGQ/0.jpg)](https://www.youtube.com/watch?v=R3DT99QllGQ "Media Library in Drupal 8.7")

We also continue to innovate on headless or decoupled Drupal. The [**JSON:API module** will ship with Drupal 8.7](https://dri.es/jsonapi-lands-in-drupal-core). I believe this not only advances Drupal's leadership in API-first, but sets Drupal up for long-term success.

These are just a few of the new capabilities that will ship with Drupal 8.7. For the complete list of new features, keep an eye out for the release announcement in a few weeks.

## Drupal 7 end of life

If you're still on Drupal 7, there is no need to panic. The Drupal community will support Drupal 7 until November 2021 — two years and 10 months from today.

After the community support ends, there will be [extended commercial support](https://www.drupal.org/project/d7es) for a minimum of three additional years. This means that Drupal 7 will be supported for at least five more years, or until 2024.

## Upgrading from Drupal 7 to Drupal 8

Upgrading from Drupal 7 to Drupal 8 can be a lot of work, especially for large sites, but the benefits outweigh the challenges.

For my keynote, I featured stories from two end-users who upgraded large sites from Drupal 7 to Drupal 8  — the [State of Georgia](https://georgia.gov/) and [Pegasystems](https://www.pega.com/).

[![State of Georgia moving from Drupal 7 to Drupal 8](https://img.youtube.com/vi/dN_zar8J2G0/0.jpg)](https://www.youtube.com/watch?v=dN_zar8J2G0 "State of Georgia moving from Drupal 7 to Drupal 8")

[![Pega Systems moving from Drupal 7 to Drupal 8](https://img.youtube.com/vi/2g7qNKsG7Mk/0.jpg)](https://www.youtube.com/watch?v=2g7qNKsG7Mk "Pega Systems moving from Drupal 7 to Drupal 8")

The keynote also featured [quietone](https://www.drupal.org/u/quietone), one of the maintainers of the [Migrate API](https://www.drupal.org/docs/8/api/migrate-api/migrate-api-overview).  She talked about the readiness of Drupal 8 migration tools.

[![Vicki 'Quietone' interview](https://img.youtube.com/vi/0C1djWansbk/0.jpg)](https://www.youtube.com/watch?v=0C1djWansbk "Vicki 'Quietone' interview")

## Preparing for Drupal 9

As announced a few months ago, [Drupal 9 is targeted for June 2020](https://dri.es/plan-for-drupal-9). June 2020 is only 14 months away, so I dedicated a significant amount of my keynote to Drupal 9.

Making Drupal updates easier is a huge, ongoing priority for the community. Thanks to those efforts, the upgrade path to Drupal 9 will be radically easier than the upgrade path to Drupal 8.

In my keynote, I talked about how site owners, Drupal developers and Drupal module maintainers can start preparing for Drupal 9 today. I showed several tools that make Drupal 9 preparation easier.  Check out [my post on how to prepare for Drupal 9](https://dri.es/how-to-prepare-for-drupal-9) for details.

![A timeline with important dates and future milestones](drupal-8-timeline-april-2019-1280w.png "Drupal 8 timeline made in April of 2019")

## Thank you

I'm grateful to be a part of a community that takes such pride in its work. At each DrupalCon, we get to see the tireless efforts of many volunteers that add up to one amazing event. It makes me proud to showcase the work of so many people and organizations in my presentations.

Thank you to all who have made this year's DrupalCon North America memorable. I look forward to celebrating our work and friendships at future events!