# Drupal launches newest version of the CMS already powering top organizations around the world

**Drupal 9 Provides Tools for Marketers, Designers and Developers to Build High-Performance Digital Experiences from Layout to Launch.**

PORTLAND, Oregon | June 3, 2020—Drupal, the most powerful enterprise open source content management system, is launching the latest—and most comprehensive—upgrade to its popular software today.

Drupal 9 comes with even more of the cutting-edge features that Drupal developers and users love. One out of every 30 websites in the world including Lufthansa, the CDC National Prevention Information Network, the European Commission, Médecins Sans Frontières/Doctors Without Borders, NASA, GRAMMYs/Recording Academy and Stanford University trust Drupal as the platform for their ambitious digital experiences.

## Drupal 9—Continuous innovation in technology and user experience

The updated version delivers powerful new features and an enhanced user experience. These features empower Drupal’s vision for the next generation of the web and include:

* Dramatically easier tools—a new layout builder, WYSIWYG media management system and content workflow tools make Drupal much easier to use. It enables users to take advantage of Drupal's powerful technical architecture more easily than ever before.

* Continuous innovation—powerful new features delivered continuously, keeping Drupal at the cutting edge of the web.

* Easiest upgrade in a decade—and Drupal’s commitment to easy upgrades in the future means never having to worry about a major replatforming to stay up to date.

> "One of the key reasons Drupal has been successful is we have always made big, forward-looking changes," says Dries Buytaert, founder and project lead of Drupal. "As a result, Drupal is one of very few CMS platforms that has stayed relevant for 20 years."

One of Drupal’s key users says they depend on Drupal’s easy-to-use interface to keep their content up-to-date—even when their developers aren’t immediately available.

In a case study about how the ACLU uses Drupal, the ACLU team says, "With Drupal's new capabilities to create and administer content, including robust media support ...[and] full layout control, without the aid of developers, ... [Drupal] empowered the ACLU to be more dynamic and responsive to external events."

While this new version makes it easier for non-developers to contribute to dynamic web platforms, it also continues to advance the underlying technology at the forefront of digital experiences—enabling developers to build the next generation of the web.

> "Drupal's API-first architecture puts it years ahead of competitors as a decoupled or headless framework,” says Tim Lehnen, chief technology officer for the Drupal Association. “Drupal can serve as the content hub for rich experiences built with the latest technologies, including modern javascript frameworks like React, Angular, or Vue, or even with emerging channels like digital assistants and AR/VR applications."

Buytaert says a key focus was making the process of upgrading as easy as possible for its users.

> "Drupal's innovation has only accelerated since the release of Drupal 8 four years ago,” says Buytaert. “Historically, major upgrades have been costly. With Drupal 9 we wanted to innovate quickly and provide an easy upgrade path from Drupal 8. We did exactly that! The upgrade from Drupal 8 to Drupal 9 is the easiest major upgrade in the last 10-plus years."

## Powered by a global community

Drupal is a true open source project, leveraging the expertise of tens of thousands of developers around the world. Drupal has earned a reputation for security, performance, accessibility and scalability that is unparalleled in the CMS ecosystem. Drupal's core strength has always been its ability to manage structured data—written once, and reused anywhere—and the Drupal community has doubled down on this with Drupal 9.

## To upgrade or get started

If you’re ready to experience Drupal, discover how easy it is to build or integrate your digital portfolio by visiting drupal.org/9.

Need some help onboarding with Drupal or building a digital experience from scratch? There are many agencies in the Drupal community that would be happy to help.

## About Drupal and the Drupal Association

Drupal is the open source content management software used by millions of people and organizations around the world, made possible by a community of 100,000-plus contributors and enabling more than 1.3 million users on Drupal.org. The Drupal Association is the non-profit organization dedicated to accelerating the Drupal software project, fostering the community, and supporting its growth.

---

For more information contact Heather Rocker,  heather@association.drupal.org
