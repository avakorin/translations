# How to Organize a Great Drupal Event

This article is designed based on a webcast I delivered to the Drupal community on the Drupal Association YouTube channel. The topic of the webcast and the article was chosen not randomly: for such an organization as Drupal, it is very important to grow and develop local communities that contribute towards global goals. That is why the webcast was quite popular. So I decided that the article will also be useful.

The webcast is attached at the end of the article.

That was a brief introduction and now we can proceed to the content of the article.

Event management is an algorithm of actions performed in order to organize a conference, a meeting, an exhibition, etc. However, the specifics of each action varies depending on a topic of events, target audiences, and other factors.

In the article, I am going to share with you the methodology of increasing the number of Drupal Events participants. As an example, I will use the Drupal Cafe events. Drupal Cafe is our locally organized 1 day Drupal educational conference. It generally combines a meetup and lectures/workshops.

## Stages of event management

### Stage 1. Current state analysis

If you decided to grow your Drupal Community and organize bigger Drupal Meetups or Drupal Camps or any other type of events, the first step is analysis. There are two main areas of research and analysis.

1. Market research
   - potential target audiences
   - stakeholders
   - competitors analysis
2. Customer satisfaction survey
   - content
   - speakers
   - place
   - etc.

Market research includes collecting and analyzing secondary data about your market potential. You need to check how many people you can attract in total or how many IT people are in your city or region. Then you need to make a list of people who might be interested in a Drupal Event except for developers. Maybe you can offer a sponsorship to certain companies. Or there are other potential partners with whom you can make a win-win or barter agreement. And last but not least, you need to gather and analyze the information about your competitors - other organizers of IT Events in your area. You can check what was good or bad during their events and learn from that.

The second thing you need to analyze is your participants. You need to combine a research of past and potential event attendees in order to find out what they want to learn at your event, what are the best date and time, what information they want to know and so on. And if you have the data about the attendees of your previous events, the most important data is their level of satisfaction. This will provide you with insights about their experience and will show the areas of crucial improvements.

### Stage 2. Planning

When you gathered all data and critically analyzed it, you can move to the planning part. It is essential to rely on facts when you plan an event.

#### Creating a vision

It is necessary to create a vision of your Drupal Event. Why are you doing it? What do you want to achieve from this event in a long-term perspective? Answer these questions and come up with a statement. It will be great if you create this vision together with your team. It will motivate all of you to do your best!

#### Setting up numerical goals

- Numerical goals
- Number of participants
- Number of applications and conversion rate

Your main goal is the number of participants. But in order to track the process of organization, you also need to have a KPI for a number of signups. That will give you an idea of how many people want to come. But usually not all of those who have registered actually come. The ratio of the number of participants and the number of signups is a conversion rate.

#### Planning the timeline

**Timeline:**

- Processes
- Responsibles
- Duration and deadlines

After we have a goal we are ready to plan the timeline of the event. There are a lot of different tools and web-services for event management but I still use a Gantt Chart in Google Docs. I believe it is the easiest way to plan a project and constantly track results. I will show the example of the Gantt Chart for Drupal Cafe that I organized in June.

![Planning of Drupal Cafe][1]

Here you can see the list of processes and the column with a person in charge of this process.

The chart contains columns for each day of a preparation period. There you mark when each process is supposed to happen and a deadline for it. I usually put the main goal and a current state on top in order to always be always aware of the progress.

### Stage 3. Implementation

In this stage, you deliver all the processes and check if KPIs are being achieved. If yes - well done. If no - sit, analyze, and re-plan! This approach to implementation allows you to react fast and be flexible.

### Stage 4. Analysis and conclusions

If you conduct researches on a regular basis it will pay you off for the next event. You will already have all the necessary information for decision making.

In this stage, you need to gather feedback from participants right after the event. You can provide them with paper or online feedback forms. Or create your own way! Sometimes we use interactive polls right during the closing session.

Also, you need to calculate an actual conversion rate and compare it with the KPI you set in the beginning. Think about what helped and what disturbed you from achieving your goal.

Those were the stages of event organizing. And now we can move to the detailed discussion of the main processes of event management.

## Processes of event management

Firstly, I'd like to tell you about the two necessary processes. Without those, you definitely won’t be able to deliver an event. I’m talking about a venue and equipment. It’s true because even if found great speakers and did amazing promotion - without well-equipped venue - there will be no event.

### Venue

If you plan a bigger Drupal Event than you usually have - check the capacity of your company’s office first. We always have Drupal Cafes in our office - it helps to position the company as well as the community. We have recently moved to a bigger place to be able to host up to 100 people. Other kinds of venue are public event venues, big conference halls etc. I’m sure if you search for such places you will find a dozen of them!

![Group photo on Drupal Event][2]

The venue should have enough space and furniture, lavatory. Check the lights before the events. Make sure that slides on a screen are visible and other places of venue are light enough at the same time.

### Equipment

You are going to need a projector and a projector screen, and a laptop. This is a minimum set. But it would much better if you also have a presentation remote, mics, and loudspeakers. And I don’t know how it’s in your countries but here people are happy to have Wi-Fi in the venue.

### Stationary and coffee-breaks

Next two processes are buying stationary and organizing coffee-breaks. That’s not that important processes if to ask the question “will this event take place or no” but they are important in terms of participants’ perception of the event. If you take care of these things on time, your participants will feel better and like your event more.

A possible list of stationery: badges and badge holders, notebooks and pens, board or flip chart and markers, and any other things you or your speakers need to make amazing sessions.

When organizing coffee-breaks you need to think not only about food but also about a disposable tableware (better to use a recyclable one) and water coolers/boilers. Prepare drinks (tea, coffee) and put sugar. You can make any menu you like. The main thing is to keep participants not hungry. Hungry people are usually upset and destructive during sessions. We usually have sandwiches, bakery, and fruits.

### Agenda management and experts experience

This process is very important. Especially in a long-term perspective. High-quality content will always bring you long-term results. And that’s what you need to think about if you plan to grow your local Drupal community. Because if you make a great promotion and a lot of people come but the sessions are boring or speakers perform not well - people’s expectation will be ruined and they won’t come again. That means that next time you’ll need to attract some other people. And from time to time it will be more and more difficult. That is why we are talking about content before talking about the promotion.

![Example stationery][3]

Some people might think that the main thing is to do great promotion and this will lead them to a great event. That is just partially true. Of course, if you prepare interesting content but attract only 10 people, it will not be effective. So both processes are important but content goes before promotion.

In this process, we need to emphasize 2 areas: **agenda development and working with speakers and experts**.  

In order to make the content of your Drupal Event engaging, understandable, and entertaining you should think about knowledge and emotional flows. Knowledge flow is a series of session linked one to another. You as agenda manager must know the content of each session to link them logically. Otherwise, the audience might get confused or lost in the middle of the event. You can start with easy to comprehend content and gradually add more difficult sessions. Or you can alternate it. But it should have a logic behind based on your vision and goals. Emotional flow is also highly important. It reflects how you delegates feel during and in between the sessions. It can help you to schedule breaks and informal discussions. For example, if the session was full of difficult technical information - you’d better put a break after to let people assimilate new knowledge and share their conclusions with friends.

Another way to plan the agenda is developing agenda blocks. You can divide all sessions into groups based on their content and explain this to delegates. That will help them to keep track of what’s going on and better understand information. For example, you can have a frontend block of sessions, then a backend block, a general block, and so on.

When your Drupal Event will get bigger you can also organize several tracks. That means that there will be several separated session flows at the same time. Usually, a conference is divided into tracks by a participants profile (newbies and professionals) or by a knowledge area (web-design track, game development track, backend track etc).

So now you are ready to develop a draft of an agenda (sessions flow, blocks). It is time to start searching for potential speakers. I use the following algorithm of finding speakers. Firstly, I think of everyone who could potentially perform at my events. I don’t know what is the situation in your city but here we have a lack of speakers specializing in IT topics. And there usually is a dilemma: either a person is a great developer but doesn’t have a public speaking experience, or they are performing great but don’t know needed topics.

That's why mostly we have speakers from our company. You can also do so. And I would advise you to organize corporate training on public speaking. It will help you to find good speakers inside your company. If your employees or colleagues are performing well at events, it will promote your company as well.

Then you’d better make a list of topics for each speaker in your list (that will fit your pre-planned agenda) in order to make a meeting with this speaker more efficient. Sometimes people agree to deliver a session but can’t choose a topic or suggest a topic that doesn’t fit your vision of the event. That is why it is much better to offer a choice from options that you have prepared instead of letting speakers choose themselves. But before making the list of topics - find out what a certain speaker usually delivers and which of those topics you can modify a bit and use for the Drupal Event.

When you approve all speakers and topics you will get your agenda done! Yey! But of course, it’s not the end of work on this process. Speakers are also people they may be overloaded or forget something. Therefore you need to keep communicating with them and remind about deadlines. And the last step here is a dry run. I know it is hard to gather all speakers together and shortly present and discuss all sessions but usually, during such discussions, great ideas are born. And you as agenda manager can check the content and give some inputs. Also dry run makes people treat preparation more seriously.

### Wow moments

This is an extra agenda thing. These are the moments when participants think “wow, never knew it before” or “never tried it”, or just “wow that’s so cool!”

People usually forget almost all information from events but that’s what they will remember, tell friends, and share on social networks. Something like, hey I’ve been to Drupal Cafe yesterday. You won’t believe what I’ve seen where - they threw out a hundred balloons! or they put zebra into the venue! or whatever else that people would want to share.

You can create wow-moments and let people spread the word about your Drupal Event. Of course, it's better to link this activity to your agenda. That will make even more effective. For example, we created an IT quiz and I asked questions between sessions and threw the tangerine to a person who gave the right answer. It added a little of gamification and people were discussing this and other companies started to use this method, too.

### Promotion

I was thinking for a long time about how to tell you about event promotion and came up with just these 3 steps.

- Develop a GTCM model
- Set KPIs for all initiatives
- Track constantly  

Well, a GTCM model is a method of planning your entire promotional activity rationally, logically, and effectively. As you probably already realized - I’m a fan of analysis, numbers, and all this stuff. That’s why this model will work only if you invest enough time in gathering and analyzing data. I told you about it at the very beginning of the article. If someone needs help with conducting a survey or with analysis - I can help, just shoot me an email.

**G stands for Goal, T - for a target audience (TA), C - for channels and M - for a Message.**

![The GTCM model][4]

## Let’s see how it works

The goal here is your event and a number of attendees you want to attract. A target audience is people with the right profile for your event. Channels are promotional channels where you will place the information about your events such as TV, social media, newspapers, and so on. And the message is what exactly you will place on those channels. How do you want to present your event and what benefits to highlight?

Your product - your event - has a set of values - characteristics or benefits for a TA and, on the other hand, the TA has its needs. In order to formulate a catchy message, you need to find the intersection between the values of your event and the needs of the TA. In other words, you need to tell your audience how your product will solve their problems or how your event will cover their needs in knowledge, communication, etc.

When you found that intersection and created the message, it's high time to decide what channels to choose for this message. There are 2 criteria for these channels. First, your TA  should use, read, or watch this channel. Second, your message should fit the channel by its meaning.

To make it easier, let’s see the example.

![Applying the GTCM model to Drupal Cafe][5]

Your project is Drupal Cafe. It has its values depending on your vision and goal. Here I put just some examples of values, which are community development, providing education on new features, tools, etc., and experience sharing. You invite specialists and students and it will be cool if they can communicate and share.

The TA of Drupal Cafe consists of several groups of people. Of course, it is Drupal developers. But that’s not it. If we want to grow the Cafe and community we need to invite other IT specialists and IT students who could get interested in Drupal after attending Drupal Cafe.

Better to define TAs by identifying their needs separately but here it’s just an example. Let's assume that the TA's needs are to get new knowledge, to have a good time, and to talk to IT specialists.

We have all needed information, great! Now the point is to find the intersection. The most obvious one is that Drupal Cafe provides education and TA wants to get new knowledge. Good, that is the intersection. The message that will indicate this should tell that Drupal Cafe provides participants with the most up to date and useful knowledge.

Next step - choosing channels. Logically it should be educational media that IT people read. The survey will help you find out what magazines or online resources your TA uses.

When you planned TA, messages, and channels, you need to set KPIs for all of your promotional activities. It is very important to track if you go according to the timeline you made at the beginning or no.

### Main KPIs for promotion

- Number of applications (and conversion)
- Number of subscribers
- Number of views

Of course, you may add other KPIs that works for you and your planned activities. But you definitely need to define it for each and every action. Yes, it takes time. But I promise if you do this for 2-3 events in a row, you will do it automatically later. As well as the GTCM model.

So set KPIs for all activities. For example, you decided to place the article on a local online news portal, you need to check how many people usually view such kind of news, and set the KPI for a number of views. And it would be great if you could approximately set the KPI for a number of signups you will get from this article and the goal for a number of people who will attend the event from this particular article. That will help you to estimate the conversion of the channel, decide which channels are most effective, and which channels you can stop using.

Social networks are the most effective promotional channel for our events. And inside of social media, the most effective is our Drupal Community group in a Russian social media - VK.

That’s why even between the Drupal Cafes when we don’t have to make promotion, we still continue working and growing this group because we know - the more people join the group - the more participants we will have on Cafe.

And finally, track your KPIs at least weekly. You need to be always aware of what’s going on and what you have achieved and what is still to go. If you realize that you are behind of your goal you can quickly replan you actions or add new initiatives to achieve the initial goal.

### Attendees servicing

That’s not the main process and therefore usually people don’t pay enough attention to it. But in a long perspective, it will make your audience loyal to your events and other activities.

Attendees servicing is making your attendees happy. You just need to treat them like they are special and do these 3 easy steps:

1. Schedule the reminder-letter.
2. Don’t hesitate to write or call personally if needed.
3. Send outputs and feedback form after the event.

## Conclusion

This is it about the stages and processes of event management that I use in ADCI Solutions for organizing Drupal Cafes. And after preparing and delivering more than 10 of them I can conclude that this algorithm works pretty well and from time to time we learn more from our experience and keep improving the methodology.

**Good luck with organizing your Drupal Events!**

---

## Contribution

Source: <https://www.adcisolutions.com/knowledge/how-organize-great-drupal-event>

Author: **Marina Paych**

Her passion for structuring processes led her to building a marketing department from scratch. Being a speaker of local and international conferences, Marina shares her experience in strategy development and talent management.

[1]: adci-198--1.jpg "The timeline of Drupal Cafe"
[2]: adci-198--2.jpg "ADCI Solutions hosts Drupal Events in its office"
[3]: adci-198--3.jpg "A notebook with branded stickers"
[4]: adci-198--4.jpg "The GTCM model"
[5]: adci-198--5.jpg "Applying the GTCM model to Drupal Cafe"
