# Drupal Paragraphs Module in Drupal 8 — A Complete Tutorial

Paragraphs is a new way of content creation. It allows the site builders to make things cleaner and can give more editing power to the end-users.

“Paragraphs” is a very popular module in drupal, used for handling content. It is similar to content fields and provides a wide range of options for designs, layout, and grouping of content as per the requirements.

## Types of Drupal Paragraphs & Its Usage?

Paragraphs can be of different types. They can be anything from an image to a video, or a simple text to a configurable slideshow.

Instead of putting all the data in one body field, we can create multiple Paragraphs Types with different structures. This allows the end user to choose between pre-defined Paragraphs Types. By using Paragraphs, we can create multiple featured Paragraphs, allowing the end users to pick the one most suitable for their needs.

The Drupal Paragraphs module is easily manageable for non-technical users, while also offering [Drupal developers](https://www.specbee.com/hire-drupal-developer) the ability to control the appearance of the elements at the theme level.

## How to use Drupal 8 Paragraphs module

### 1. Install and Enable Drupal Paragraphs module.

![drupal paragraph module](drupal-paragraphs-module-install.jpg)

Drupal Paragraphs module requires [Entity Reference Revision](https://www.drupal.org/project/entity_reference_revisions) module. To work with drupal paragraph module, install and enable Entity Reference Revision module.

![drupal paragraph field](drupal-paragraph-field-module.jpg)

###  2. Create a new Paragraphs Type

- To take the advantages of Drupal Paragraphs module, create at least one Paragraphs Types.
- Navigate to Structure > Paragraphs types.
- Click on “Add paragraph types”.
- On the next page, there are three fields to be filled.  Label, Paragraph type icon and description. The Label field(mandatory), is the name given to the paragraph type created. If required, icon and the description of the paragraph can be given.

![add-paragraph-module](add-paragraph-module.jpg)

- After editing, click on “save and manage fields”.
- In manage fields, click on “Add field”.
- Here you can add more fields as per the requirement. These fields include text fields, image fields, boolean etc.. This is similar to adding fields in content type.
- After adding the field, click on “save and continue”.
- On clicking “save and continue”, you will be guided to the “Field settings” tab. For any of these fields, there are settings such as, maximum length and allowed number of values. This is useful to allow more than one value for some fields when it is required. 

![paragraph-module](maximize-paragraph-length.jpg)

- Click on “save field settings”.
- In the next tab we can set the title for the field which is displayed when the new content is created.
- Then click on “save settings”.
- Now the field created can be seen inside Paragraphs Types.

![manage field in paragraphs modules](manage-field-drupal-module.jpg)

###  3. Add Paragraphs to the content type:

- Navigate to Structure > Content type. Choose the content type for which the created paragraph is required.
- Go to “manage fields” of the content type and click “add field”.
- To use Paragraphs, open the “Select a field type” dropdown list and select “Paragraph” under “Reference revisions”. After selecting “Paragraph” give a label name for the field which is used while creating the content. After labeling, click on “save and continue”.
- On the next page, there are two settings “Type of item to reference” and “Allowed number of values”. “Type of item to reference” should be set to “Paragraph” and under “number of values”, it is better to set it to “Unlimited” so that we can value innumerable times. Click “Save field settings”.

![type of the paragraphs-module](type-of-the-paragraphs-module.jpg)

- Clicking “Save field settings” will take us to the next tab where there are options to choose the paragraphs type to be used in this content type. If we want a particular paragraph type to be used, check on the paragraph that is required. Else, click ”Save settings” without checking the paragraph types. This will give dropdown during the content creation and we can use any paragraphs that are created.
   

![reference-type](reference-type.jpg)

- By clicking “Save settings” we can see the field with type entity reference revisions.

### 4. Adding contents to the content type having drupal paragraphs.

- Go to Content > Add content, and choose the content type to which paragraph is added.
- You will see a paragraph field with all the fields added to the paragraphs. There are two more options: They are “remove” and “Add (title of the field referred to the paragraph)”. To add multiple values of the same structure, click on add, and to remove paragraph, click on remove.

![demo-paragraph-module](demo-paragraph-module.jpg)

## Features of Paragraphs module

1. **Allows the editor to create different structures on each page.**
   If there are different structures on the same page or different pages, paragraphs can be used. For ex. There is an image with text on the top and slideshow on the bottom. We can use Paragraphs to make this less complicated.
2. **Allows the editor to change the order of the paragraphs.**
   If there is a change in display, like there is an image on the top followed by title and description, and you want to change it to title to the top followed by image and description. Such changes can be done using paragraphs easily. 
   Go to “manage display” of the paragraphs used and change the order, which will change the display order of all the content wherever the paragraph is used.
3. **Paragraphs can be nested**
   One paragraph field can be referred to in another paragraph field. While selecting field to paragraphs, select “Paragraph” under “Reference revisions” and select which paragraphs type you want to add.

------

Source: [Drupal Paragraphs Module in Drupal 8 - A Complete Tutorial](https://www.specbee.com/blogs/drupal-paragraphs-module-drupal-8-complete-tutorial)

Translated by: [madt](https://drupal.ru/username/madt)