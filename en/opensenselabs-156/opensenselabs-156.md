# Why is Drupal great for multilingual sites

World Economic Forum, an International Organization for Public/Private Cooperation, has its digital presence not only in English language but other prominently spoken languages like Spanish, Chinese, French and Japanese as well. Rio Olympics 2016, which is also known for the infamous haul of 9 golds by Usain Bolt, had its online presence in both English and French. Oxfam, an international confederation of 19 organizations that has the objective of mobilizing the power of people against poverty, has its website in Spanish and French languages other than English. What is common between all of them? Their websites are powered by [Drupal][1] which is one of the leaders in the open source content management system (CMS) market.

![Illustration showing people in the background and different coloured leaves containing the word Hello in different languages][2]

Built and maintained by an international community of developers, Drupal has been a marvelous solution for organizations around the globe that are in need of swiftly launching their websites that is tailored to a variety of language needs. As a matter of fact, Drupal 8, the latest version, was created keeping multilingual use in mind. But why should an organization consider building a multilingual site in the first place? Let’s understand its significance before taking a plunge in exploring Drupal’s capabilities in building a localized site.

## Significance of multilingual sites

![A bar graph showing percentage of online users of different languages][3]

English is certainly the most widely spoken language in the world and dominates the internet space. But it would be wrong to consider it is as the most preferred language. Chinese, Spanish, Arabic and many others, as you can see in the graph above, are spoken by millions of online users. So, optimizing your site and going multilingual gives you a huge advantage.

Some of the major benefits of having a multilingual website are:

![Illustration showing horizontal bars to explain benefits of multilingual website][4]

One of the most important benefits of going multilingual is that you can enhance communication as even though English has its preeminence over the online space. That doesn’t mean that everyone wants to buy from English language websites. Research from [CSA Research][5] says that people prefer to make purchases while browsing in their own native language as more than half of the people who were surveyed bought from sites that were available in their own language. Another important benefit is that you can expand your reach and cover a wider audience. Localized websites also result in increased client satisfaction. Multilingual SEO is of paramount importance too and localising your website into different languages helps improve your SERP (Search Engine Results Page) ranking locally across the globe.

Moreover, website localization is efficacious and gives you a competitive advantage. In a survey by [Content Marketing World][5], 60% of participating marketers agreed that there is a dearth of multilingual content marketing strategies. Therefore, not capitalizing on the immense amount of opportunities that website localization presents can prove detrimental to your business pursuits. You may lose out to a local business that is well known and also fare badly against another foreign company that is localising better. Also, not only you get to touch a wider demographic of the audience, you get the opportunity to increase your audience as you will be noticed by a massive pool of potential buyers for your product and services. And you do not get penalized for duplicated content on translated sites. Localising your website also bring about higher conversions and skyrocketed return on investment. Even if you cannot please everyone, you can please the majority of them when you make your site multilingual.

## Considerations for turning your site multilingual

In order to develop a great multilingual site, there are some considerations that should be kept in mind.

Building a site in any language needs a clearly thought-out plan for content and needs a detailed picture of your business objectives and vision. For instance, you should know who the ideal customer for your products and services is and where does he or she live. And you should know which language will have a wider reach amongst your potential customers.

You would, then, require a wisely laid-out strategy. You can register top-level domains like .mx for Mexico or .fr for France. You must know that photographs, artwork, fonts, and colour choices carry different meanings across different cultures. Visitors should find it easy to navigate and find their native language microsite. Different languages should be used appropriately and nothing should come across as offensive or insensitive to the different cultures.

Most importantly, you need to determine the best technological solution for creating your localized site. A robust CMS like Drupal can be a remarkable option that can dovetail with your multilingual strategy.

## Drupal in the mix

If you are in the need of quickly creating customized sites in any language of your choice or an intricate multilingual web application with dynamic, language-based displays, Drupal is a wonderful option. Without the need for any additional components, Drupal 8 can be installed in over 90+ languages. Drupal’s out-of-the-box support for language handling assists you in delivering localized digital experiences and saves time and money. Drupal 8 core comes with four core modules for localising content on the website.

### Administering Language

![Language handling module][6]

The Language handling module lets you pick your choice from 94 languages.

Language configuration has been streamlined. Assign a language to everything from taxonomy terms to administration language. You can even delete the English language. Each user can also select his or her own language for the admin interface.

The detection of browser language can be easily configured with external language codes. There is also a built-in transliteration for the machine names.

### Translating Interface

![Interface translation module][7]

Interface translation module provides a central directory to manage interface. It has built-in translation UI for simplifying content editing. By allowing automatic downloads and updates, it lets users use any translation interface available in the Drupal community in any language supported by Drupal 8.

English language interface can be customized. You do not have to use English as your default language.

### Translating Content

![Content translation module][8]

Content translation module is applicable to all of your content. It lets you translate everything from the pages to the taxonomy terms. It allows you to configure custom fields of a web content.

Like interface translation, the default language of your content can be flexibly configured. You can even hide or display the position of language selector.

### Managing Configuration

![Configuration translation module][9]

Everything that comes with the configuration of your website can be translated using this module. Things like views, blocks, panels, field panels or text formats can be easily translated with its built-in responsive translation interface.

Moreover, there is a provision of an overview screen to help you in the process.

## Case studies

![Homepage of Sevilla FC website with three people holding the team jersey on left and the football team posing for the camera on right][10]

Sevilla FC, founded in 1890, is one of the oldest football teams in Spain. A digital agency helped it to implement a series of quality and quantitative enhancement in its digital services via web, mobile and social media channels. Drupal turned out to be a fantastic option in their pursuit of improving the digital experience. Drupal’s in-built multilingual capabilities, top-of-the-line [open source security][11], tremendous [scalability][12], great content workflow tools and the support for [agile project delivery][13] were the major factors that made it the right choice for this project. With a continuous increase in the number of international fanbases of Sevilla FC, it was important to create a multilingual site. The multilingual capability of Drupal 8 streamlined the process of localising their website into multiple languages.

What if your native language is not available to choose from 90+ languages that the Drupal offers? OpenSense Labs leveraged Drupal 8 to provide a clean architecture and design for the betterment of digital presence of the Ministry of Finance in Somalia.

![Homepage of Ministry of Finance of Somalia with an image of a multi floor building][14]

The remodelling of the website needed new content types with user-friendly navigation and the addition of a custom type of publication to publish annual reports. Agile development methodology helped the project to be completed within six weeks. And, as Somali isn’t available in the list of languages provided by Drupal 8’s out of the box solution, the flexible multilingual infrastructure of Drupal aided in adding it easily.

## Conclusion

While English language internet users have been the primary source of the target of every business. There has been an unprecedented growth in the non-English language online users as well. Reaching them out is beneficial for widening your customer base and establishing their business in newer markets. Your content determines the website traffic, customer retention, and conversion rates. CMS is the warehouse of your web content. It is significant to choose the right CMS before even making your first move towards making your site multilingual.

Drupal 8 has made tremendous improvements to include the built-in multilingual feature. It delivers multilingual websites right out of the box. Its 4 key modules for language, interface, content, and configuration of your website helps in efficaciously building a multilingual site.

[1]: https://opensenselabs.com/blog/articles/drupal-web-development-project "Why Choose Drupal For Your Next Web Development Project"
[2]: opensenselabs-156--1.png "Illustration showing people in the background and different coloured leaves containing the word Hello in different languages"
[3]: opensenselabs-156--2.png "A bar graph showing percentage of online users of different languages"
[4]: opensenselabs-156--3.png "Illustration showing horizontal bars to explain benefits of multilingual website"
[5]: https://www.searchenginepeople.com/blog/925-multilingual-websites.html "Multilingual Websites: Benefits And Best Practices"
[6]: opensenselabs-156--4.png "Language handling module"
[7]: opensenselabs-156--5.png "Interface translation module"
[8]: opensenselabs-156--6.png "Content translation module"
[9]: opensenselabs-156--7.png "Configuration translation module"
[10]: opensenselabs-156--8.png "Homepage of Sevilla FC website with three people holding the team jersey on left and the football team posing for the camera on right"
[11]: https://opensenselabs.com/blog/articles/open-source-security-drupal "Leverage Open Source Security In Drupal"
[12]: https://opensenselabs.com/blog/articles/scalable-cms-drupal-8 "Most scalable CMS: Drupal 8"
[13]: https://opensenselabs.com/blog/articles/agile-boon-project-management "Agile: A boon for project management"
[14]: opensenselabs-156--9.png "Homepage of Ministry of Finance of Somalia with an image of a multi floor building"
